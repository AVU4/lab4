#include <stdio.h>
#include <malloc.h>

typedef struct list {
	int value;
	struct list* next;
}l_list;

l_list* list_add_front(l_list* head, int new_value){
	l_list* new_elem = (l_list*) malloc(sizeof(l_list));

	new_elem->value = new_value;
	new_elem->next = head;

	return new_elem;

}

l_list* create_list() {
	l_list* head = NULL;
	size_t i;
	size_t cnt;
	scanf("%zu", &cnt );
	int value;
	for (i = 0; i < cnt; i ++) {
		scanf("%d", &value);
		head = list_add_front(head, value);
	}
	return head;
}

void print_list(l_list* head) {
	l_list* current = head;

	while (current != NULL) {
		printf("%d ", current->value);
		current = current->next;
	}
}

void list_add_back(l_list* head, int new_value) {
	l_list* new_elem = (l_list*) malloc(sizeof(l_list));
	l_list* current = head;
	
	new_elem->value = new_value;
	new_elem->next = NULL;

	while (current->next != NULL){
		current = current->next;
	}
	current->next = new_elem;

}

int list_length(l_list* head) {
	int length = 0; 
	l_list* current = head;

	while (current != NULL){
		current = current->next;
		length ++;
	}
	return length;
}

int list_sum(l_list* head) {
	int sum = 0;
	l_list* current = head;

	while(current != NULL){
		sum += current->value;
		current = current->next;
	}
	return sum;
}

l_list* list_node_at(l_list* head, size_t index){
	size_t current_index = 0;
	l_list* current = head;

	while (current != NULL && current_index != index){
		current_index ++;
		current = current->next;
	}
	if (index == current_index)
	       	return current;
	else
		return NULL;
}

int list_get(l_list* head, size_t index){
	size_t current_index = 0;
	l_list* current = list_node_at(head, index);
	if (current == NULL) return 0;
	else return current->value;
}

void list_set_value(l_list* head, size_t index, int new_value) {
	size_t current_index = 0;
	l_list* current = list_node_at(head, index);
	if (current != NULL){
		current->value = new_value;
	}
}

void list_free(l_list* head) {
	while (head != NULL){
		l_list* next = head->next;
		free(head);
		head = next;
	}
}
