#include "linked_list.h"
#include <stdio.h>
#include <stddef.h>
#include <malloc.h>

int main( void) {
	l_list* array;
	printf("Создание связанного списка\n");
	array = create_list();
	printf("%d\n", array->value);
	print_list(array);
	printf("\n");
	printf("Добавление в начало нового элемента\n");
	array = list_add_front(array, 6);
	print_list(array);
	printf("\n");
	printf("Добавление в конец нового элемента\n");
	list_add_back(array, 7);
	print_list(array);
	printf("\n");
	printf("Длина linked list\n");
	printf("%d\n", list_length(array));
	printf("Сумма элементов\n");
	printf("%d\n", list_sum(array));
	printf("Введите индекс\n");
	int index;
	scanf("%d", &index);
	printf("%d\n", list_get(array, index ));
	list_free(array);
	printf("Память освобождена\n");
	return 0;
}
